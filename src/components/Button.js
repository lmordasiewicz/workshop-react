import React, { Component } from 'react'

class Button extends Component {
    render() {
        const { onChange, text } = this.props
        return <button type="button" onChange={onChange}> { text } </button>
    }
}

export default Button