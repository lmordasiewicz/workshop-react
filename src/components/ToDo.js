import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './ToDo.scss';

class ToDo extends Component {
    state = {
        loaded: false
    }

    componentDidMount() {
        setTimeout(() => this.setState({ loaded: true }), 100)
    }

    render() {
        return <li className={`todo ${this.state.loaded ? "loaded" : ""}`}>{this.props.item.text}</li>
    }
}

ToDo.propTypes = {
    item: PropTypes.object
};

ToDo.defaultProps = {
    item: {}
};

export default ToDo;
