import React, { Component } from 'react'

class Input extends Component {
    render() {
        const { onChange, value } = this.props
        return <input type="text" onChange={onChange} value={value}/>
    }
}

export default Input