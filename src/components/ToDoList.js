import React from 'react';
import PropTypes from 'prop-types';
import ToDo from './ToDo';

const ToDoList = ({ list }) => (
    !list.length
      ? <p style={{color: 'green'}}>no todos</p>
      : <ol>
      {
        list
          .map(item => 
            <ToDo key={item.id} item={item} />
        )
      }
    </ol>
);

ToDoList.propTypes = {
    list: PropTypes.array
};

ToDoList.defaultProps = {
    list: []
};

export default ToDoList;
