import React from 'react';
import { linkTo } from '@storybook/addon-links';
import { Welcome } from '@storybook/react/demo';



export default {
  title: 'Welcome',
  component: Welcome,
};

const Children = ({ name = 'haha', date}) => 
  <p> I am Children</p>


const Parent = ({ children }) => <p>I am { children }</p>

class MyComponent extends React.Component {
  state = { 
    counter: 0,
    name: this.props.name
  }

  onClick = () => {
    this.setState(prevState => ({ counter: prevState.counter + 1}))
  }

  onChange = e => {
    const { value } = e.target
    this.setState(({ name: value }))
  }

  render() {
    return (
      <React.Fragment>
        <p onClick={this.onClick}>
          {this.state.name} Clicked { this.state.counter } times
        </p>
        <input type="text" onChange={this.onChange}/>
      </React.Fragment>
    )
  }
}

export const ToStorybook = () => 
  <div>
    <Parent>Marta</Parent>
    <Parent children="Marta"></Parent>
    <MyComponent name="marta"/>
  </div>;


ToStorybook.story = {
  name: 'to Storybook',
};
