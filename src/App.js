import React, { Component } from 'react';

import './components/Button.scss'
import ToDoList from './components/ToDoList';

class App extends Component {
  state = {
    toDoList:[],
    newToDo: { text: '' },
  }

  onNewToDoChange = e => {
    const { value } = e.target
    const newToDo = { 
      text: value
    }
    this.setState({newToDo})
  }

  onAddToDo = () => {
    const text = this.state.newToDo.text
    if (!text) return

    const newToDo = {
      text: text,
      id: new Date().getTime()
    }

    this.setState(prevState => ({
      toDoList: [...prevState.toDoList, newToDo],
      newToDo: { text: '' }
    }))
  }

  render() {
    return (
      <div className="App">
        <h1> TO Do list</h1>
        <ToDoList list={this.state.toDoList} />
        <input type="text" onChange={this.onNewToDoChange} value={this.state.newToDo.text}/>
        <button className="btn" type="button" onClick={this.onAddToDo}> Add </button>
      </div>
    )
  }
}

export default App;
